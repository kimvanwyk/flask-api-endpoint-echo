FROM registry.gitlab.com/kimvanwyk/python3-flask-poetry:main

COPY ./flask_api_endpoint_echo/*.py /app/
ENV MODULE_NAME=app
