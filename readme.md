# Flask API Endpoint Echo

Simple flask app to echo any data sent to an endpoint. The code is heavily borrowed from [this project](https://github.com/cuttlesoft/flask-echo-server).

The app will listen to any endpoint for any method and print out the data sent to it.

It is also built into a Docker image at [registry.gitlab.com/kimvanwyk/flask-api-endpoint-echo:main](registry.gitlab.com/kimvanwyk/flask-api-endpoint-echo:main). It requires no arguments.




